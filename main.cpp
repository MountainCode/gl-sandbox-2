#include <iostream>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


static float ticker = 0.0;
float xAngle = 0.0f;
float yAngle = 0.0f;
float red = 1.0f;
float green = 1.0f;
float blue = 1.0f;

static void DrawCube(void) {
  glLoadIdentity();
  glRotatef(xAngle, 0.0f, 1.0f, 0.0f);
  glRotatef(yAngle, 1.0f, 0.0f, 0.0f);
  // gluLookAt(10, 10, 10, 0, 0, 0, 0, 1, 0);
  glBegin(GL_QUADS);

  glutWireCube(0.5);
  glEnd();
  glFlush();
}

static void renderScene(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // glLoadIdentity();
  // gluLookAt(
  //   0.0f, 0.0f, 10.0f,
  //   0.0f, 0.0f, 0.0f,
  //   0.0f, 1.0f, 0.0f
  // );
  // glRotatef(xAngle, 0.0f, 1.0f, 0.0f);
  // glColor3f(red, green, blue);
  DrawCube();
  // glBegin(GL_TRIANGLES);
  //   glVertex3f(-0.5, -0.5, 0.0);
  //   glVertex3f(0.5, 0.0, 0.0);
  //   glVertex3f(0.0, 0.5, 0.0);
  // glEnd();

  // xAngle += 0.01f;
  glutSwapBuffers();
}

static void changeSize(int w, int h) {
  // Prevent a divide by zero when window is too short.
  if(0 == h)
    h = 1;
  float ratio = (float) w / h;
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, w, h);
  // gluPerspective(45, ratio, 1, 1000); this breaks things.
  glMatrixMode(GL_MODELVIEW);
}

static void processNormalKeys(unsigned char key, int x, int y) {
  if(27 == key)
      exit(0);
  else if('r' == key) {
    int mod = glutGetModifiers();
    if(GLUT_ACTIVE_ALT == mod)
      red = 0.0;
    else
      red = 1.0;
  }
}

static void processSpecialKeys(int key, int x, int y) {
  switch(key) {
    case GLUT_KEY_F1:
      if(glutGetModifiers() == GLUT_ACTIVE_CTRL|GLUT_ACTIVE_ALT)
        red = 1.0; green = 0.0; blue = 0.0;
      red = 1.0;
      green = 0.0;
      blue = 0.0;
      break;
    case GLUT_KEY_F2:
      red = 0.0;
      green = 1.0;
      blue = 0.0;
      break;
    case GLUT_KEY_F3:
      red = 0.0;
      green = 0.0;
      blue = 1.0;
      break;
  }
}

float deltaXAngle = 0.0f;
float deltaYAngle = 0.0f;
int xOrigin = -1;
int yOrigin = -1;

static void mouseButton(int button, int state, int x, int y) {
  if(GLUT_LEFT_BUTTON == button) {
    if(GLUT_UP == state) {
      // xAngle += deltaXAngle;
      xOrigin = -1;
      yOrigin = -1;
    }
    else {
      xOrigin = x;
      yOrigin = y;
    }
  }
}

static void mouseMove(int x, int y) {
  if(xOrigin >= 0) {
    deltaXAngle = (x - xOrigin) * 0.001f;
    xAngle += deltaXAngle * 25;
  }
  if(yOrigin >= 0) {
    deltaYAngle = (y - yOrigin) * 0.001f;
    yAngle += deltaYAngle * 25;
  }
}

static void timer(void) {
  float dtime = ticker;
  ticker = glutGet(GLUT_ELAPSED_TIME) / 500.0;
  dtime = ticker - dtime;

  glutPostRedisplay();
}

int main(int argc, char** argv) {
  setenv("DISPLAY", ":0.0", false);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE | GLUT_DEPTH);
  glutInitWindowSize(1024, 1024);
  glutInitWindowPosition(10, 10);
  glutCreateWindow("OpenGL");

  glutDisplayFunc(renderScene);
  glutReshapeFunc(changeSize);
  glutIdleFunc(timer);
  glutKeyboardFunc(processNormalKeys);
  glutSpecialFunc(processSpecialKeys);

  glutMouseFunc(mouseButton);
  glutMotionFunc(mouseMove);

  glutMainLoop();

  return 1;
}
