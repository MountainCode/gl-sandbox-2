TARGET = opengl
CC = g++
LIBS = -lglut -lGL -lGLU

all: opengl

opengl:
	$(CC) -o $(TARGET) main.cpp $(LIBS)

clean:
	rm -rf $(TARGET)
